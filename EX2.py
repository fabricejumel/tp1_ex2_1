#!/usr/bin/env python
# coding: utf-8

class SimpleCalculator:

    def sum(self, a, b ):
        return(a+b);

    def substract(self,a, b):
        return(a-b);

    def multiply(self,a, b):
        return(a*b);

    def divide(self,a, b):
        return(a/b);



############# Test functions ############

a=5;
b=7;
s=SimpleCalculator();
print ("We will test  5+7, 5-7, 5*7 et 5/7");
print(s.sum(a,b));
print(s.substract(a,b));
print(s.multiply(a,b));
print(s.divide(a,b));

